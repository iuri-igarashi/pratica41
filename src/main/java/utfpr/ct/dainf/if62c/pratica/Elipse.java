/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author xadu
 */
public class Elipse {
    public double eixo_x, eixo_y;

    public Elipse(double semieixo_x, double semieixo_y) {
        eixo_x = semieixo_x * 2;
        eixo_y = semieixo_y * 2;
    }

    public Elipse() {
        
    }
    
    public double getArea(){
        double r = eixo_x / 2;
        double s = eixo_y / 2;
        return Math.PI * r * s;
    }
    
    public double getPerimetro(){
        double r = eixo_x / 2;
        double s = eixo_y / 2;
        return Math.PI * ( (3*(r+s)) - Math.sqrt(((3*r)+s) * (r+(3*s))) );
    }
    
}
